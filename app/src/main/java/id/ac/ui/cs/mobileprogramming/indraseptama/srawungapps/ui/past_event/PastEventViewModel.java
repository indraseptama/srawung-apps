package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.past_event;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.KegiatanRepository;

public class PastEventViewModel extends AndroidViewModel {
    // TODO: Implement the ViewModel
    private LiveData<List<Kegiatan>> mKegiatan;
    private KegiatanRepository mRepo;

    public PastEventViewModel(@NonNull Application application) {
        super(application);
        mRepo = new KegiatanRepository(application);
        mKegiatan = mRepo.getPastKegiatan();
    }

   public LiveData<List<Kegiatan>> getAllKegiatans(){
        return mKegiatan;
   }
}
