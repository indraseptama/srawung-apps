package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.kampung;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kampung;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.User;

public class KampungkuFragment extends Fragment {

    private KampungkuViewModel mViewModel;
    private TextView namaKampung;
    private TextView ketuaRT;
    private TextView ketuaPemuda;
    private TextView alamat;
    private TextView wargas;
    public static KampungkuFragment newInstance() {
        return new KampungkuFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_kampungku, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        namaKampung = getView().findViewById(R.id.namakampung);
        ketuaRT = getView().findViewById(R.id.namaKetuaRT);
        ketuaPemuda = getView().findViewById(R.id.namaKetuaPemuda);
        alamat = getView().findViewById(R.id.alamat);
        wargas = getView().findViewById(R.id.warga);


        mViewModel = ViewModelProviders.of(this).get(KampungkuViewModel.class);
        mViewModel.getKampung("1").observe(this, new Observer<Kampung>() {
            @Override
            public void onChanged(Kampung kampung) {
                namaKampung.setText(kampung.getName());
                alamat.setText(kampung.getAlamat());
                for(Integer id : kampung.getIdWargas()){
                    mViewModel.getWargas(String.valueOf(id)).observe(KampungkuFragment.this, new Observer<User>() {
                        @Override
                        public void onChanged(User user) {
                            if(user.getId() == kampung.getIdKetuaRT()){
                                ketuaRT.setText(user.getName());
                            }
                            if (user.getId() == kampung.getIdKetuaPemuda()){
                                ketuaPemuda.setText(user.getName());
                            }
                            wargas.append(user.getName() + "\n");
                        }
                    });
                }
            }
        });
    }

}
