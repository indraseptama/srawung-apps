package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.add_event_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.HomeActivity;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.databinding.ActivityAddEventBinding;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;

public class AddEventActivity extends AppCompatActivity {
    final Calendar myCalendar = Calendar.getInstance();
    private static final String TAG = "AddEventActivity";
    private AddEventViewModel addEventViewModel;
    private ActivityAddEventBinding addEventBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addEventViewModel = ViewModelProviders.of(this).get(AddEventViewModel.class);
        addEventBinding = DataBindingUtil.setContentView(AddEventActivity.this, R.layout.activity_add_event);
        addEventBinding.setLifecycleOwner(this);
        addEventBinding.setAddEventViewModel(addEventViewModel);

        addEventBinding.inputHari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddEventActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        addEventBinding.inputJam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(AddEventActivity.this, time, myCalendar
                        .get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), true).show();
            }
        });

        addEventViewModel.getKegiatan().observe(this, new Observer<Kegiatan>() {
            @Override
            public void onChanged(Kegiatan kegiatan) {
                if(!isInputValid(kegiatan)){
                    if(TextUtils.isEmpty(kegiatan.getName())){
                        addEventBinding.inputNama.setError(getString(R.string.errorInputNamaKegiatan));
                    }
                    if(TextUtils.isEmpty(kegiatan.getShortDescription())){
                        addEventBinding.descInput.setError(getString(R.string.errorInputDeskripsiKegiatan));
                    }
                    if(TextUtils.isEmpty(kegiatan.getReason())){
                        addEventBinding.inputAlasan.setError(getString(R.string.errorInputAlasan));
                    }
                    if(TextUtils.isEmpty(kegiatan.getPlace())){
                        addEventBinding.inputTempat.setError(getString(R.string.errorInputtempat));
                    }
                    if(kegiatan.getDate()==null){
                        checkStringEmpty(addEventBinding.inputHari,
                                addEventBinding.inputHari.getText().toString(),
                                getString(R.string.errorInputHari));
                        checkStringEmpty(addEventBinding.inputJam,
                                addEventBinding.inputJam.getText().toString(),
                                getString(R.string.errorInputJam));
                    }
                    if(kegiatan.getSasaran() == null || kegiatan.getSasaran().isEmpty()){
                        addEventBinding.sasaranTextView.setError(getString(R.string.errorInputSasaran));
                    }
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddEventActivity.this);

                    builder
                            .setMessage(getString(R.string.dialogConfirmation))
                            .setPositiveButton(getString(R.string.Ya),  new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    new insertKegiatanTask().execute(kegiatan);
                                }
                            })
                            .setNegativeButton(getString(R.string.Tidak), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            }
        });

        addEventViewModel.nama.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                checkStringEmpty(addEventBinding.inputNama, s, getString(R.string.errorInputNamaKegiatan));
            }
        });

        addEventViewModel.deskripsiSingkat.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                checkStringEmpty(addEventBinding.descInput, s, getString(R.string.errorInputDeskripsiKegiatan));
            }
        });

        addEventViewModel.hari.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                checkStringEmpty(addEventBinding.inputHari, s, getString(R.string.errorInputHari));
            }
        });

        addEventViewModel.jam.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                checkStringEmpty(addEventBinding.inputJam, s, getString(R.string.errorInputJam));
            }
        });

        addEventViewModel.tempat.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                checkStringEmpty(addEventBinding.inputTempat, s, getString(R.string.errorInputtempat));
            }
        });

        addEventViewModel.sasaran.observe(this, new Observer<List<TipeWarga>>() {
            @Override
            public void onChanged(List<TipeWarga> tipeWargas) {
                if (tipeWargas.size() == 0) {
                    addEventBinding.sasaranTextView.setError("error");
                } else {
                    addEventBinding.sasaranTextView.setError(null);
                }
            }
        });

        addEventViewModel.alasan.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                checkStringEmpty(addEventBinding.inputAlasan, s, getString(R.string.errorInputAlasan));
            }
        });

    }

    private void checkStringEmpty(TextView textView, String s, String error) {
        if (TextUtils.isEmpty(s)) {
            textView.setError(error);
            textView.requestFocus();
        }
        else{
            textView.setError(null);
        }
    }

    private boolean isInputValid(Kegiatan kegiatan) {
        if (TextUtils.isEmpty(kegiatan.getName()) || TextUtils.isEmpty(kegiatan.getPlace()) ||
                TextUtils.isEmpty(kegiatan.getReason()) || TextUtils.isEmpty(kegiatan.getShortDescription()) ||
                kegiatan.getDate() == null || kegiatan.getSasaran().size() < 1) {
            return false;
        }
        return true;
    }

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabelDate();
        }
    };

    private TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            myCalendar.set(Calendar.MINUTE, minute);
            updateLabelHour();
        }
    };

    private void updateLabelDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        addEventBinding.inputHari.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabelHour() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        addEventBinding.inputJam.setText(sdf.format(myCalendar.getTime()));
    }


    class insertKegiatanTask extends AsyncTask<Kegiatan, AddEventViewModel, Void> {
        @Override
        protected Void doInBackground(Kegiatan... kegiatans) {
            addEventViewModel.insert(kegiatans[0]);
            Intent intent = new Intent(AddEventActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return null;
        }
    }
}