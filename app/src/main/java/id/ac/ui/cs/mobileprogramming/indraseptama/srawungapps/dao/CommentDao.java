package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;

@Dao
public interface CommentDao {
    @Insert
    void insert(Comment comment);

    @Update
    void update(Comment comment);

    @Delete
    void delete(Comment comment);

    @Query("DELETE FROM comment_table")
    void deleteAllComment();

    @Query("SELECT * FROM comment_table WHERE id=:id")
    LiveData<Comment> getCommentById(String id);

    @Query("SELECT * FROM comment_table")
    LiveData<List<Comment>> getAllComents();

}