package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.Locale;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.detail_event_activity.DetailActivity;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.add_event_activity.AddEventActivity;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.detail_kegiatan.DetailKegiatanFragment;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.MyComunicator;

public class HomeActivity extends AppCompatActivity implements MyComunicator {
    private BottomNavigationView navView;
    private static final String TAG = "HomeActivity";
    private boolean islLandscape;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Locale id = new Locale("in", "ID");
        Locale.setDefault(id);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home,R.id.navigation_past_agenda, R.id.navigation_kampungku,
                R.id.navigation_profile)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        islLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        findViewById(R.id.addButton).setOnClickListener(moveToAddActivity);
    }

    @Override
    public void displayDetails(Kegiatan mKegiatan) {
        if(islLandscape && mKegiatan !=null){
            NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
            Fragment eventFragment = navHostFragment.getChildFragmentManager().getFragments().get(0);
            DetailKegiatanFragment detailKegiatanFragment = (DetailKegiatanFragment) eventFragment
                    .getChildFragmentManager()
                    .findFragmentById(R.id.fragment_DetailKegiatan);
            detailKegiatanFragment.displayDetails(mKegiatan);
        }
        else{
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra("kegiatanId", String.valueOf(mKegiatan.getId()));
            startActivity(intent);
        }
    }

    public OnClickListener moveToAddActivity = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(HomeActivity.this, AddEventActivity.class );
            startActivity(intent);
        }
    };
}
