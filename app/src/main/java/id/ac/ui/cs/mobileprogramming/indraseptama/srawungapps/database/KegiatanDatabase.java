package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.CommentDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.KampungDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.KegiatanDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.UserDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kampung;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.User;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;

@Database(entities = {Kegiatan.class, Kampung.class, User.class, Comment.class}, version = 1, exportSchema = false)
public abstract class KegiatanDatabase extends RoomDatabase {
    private static KegiatanDatabase instance;

    public abstract KegiatanDao kegiatanDao();
    public abstract KampungDao kampungDao();
    public abstract UserDao userDao();
    public abstract CommentDao commentDao();

    public static synchronized KegiatanDatabase getInstance(Context context){
        if(instance==null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    KegiatanDatabase.class, "kegiatan_table")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return  instance;
    }

    private  static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private KegiatanDao kegiatanDao;
        private KampungDao kampungDao;
        private CommentDao commentDao;
        private UserDao userDao;
        private PopulateDbAsyncTask(KegiatanDatabase kegiatanDatabase){
            kegiatanDao = kegiatanDatabase.kegiatanDao();
            kampungDao = kegiatanDatabase.kampungDao();
            commentDao = kegiatanDatabase.commentDao();
            userDao = kegiatanDatabase.userDao();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
            Date date1 = new Date();
            Date date2 = new Date();
            Date date3 = new Date();
            Date date4 = new Date();


            try {
                date1 = sdf.parse("28-12-2019 10:00:00");
                date2 = sdf.parse("06-11-2019 15:00:00");
                date3 = sdf.parse("30-12-2019 10:00:00");
                date4 = sdf.parse("29-01-2020 10:00:00");

            } catch (ParseException e) {
                e.printStackTrace();
            }

            List<TipeWarga> sasaran1 = new ArrayList<>();
            sasaran1.add(TipeWarga.ANAK_ANAK);
            sasaran1.add(TipeWarga.IBU_IBU);
            sasaran1.add(TipeWarga.BAPAK_BAPAK);
            sasaran1.add(TipeWarga.PEMUDA);

            List<TipeWarga> sasaran2 = new ArrayList<>();
            sasaran2.add(TipeWarga.PEMUDA);

            List<TipeWarga> sasaran3 = new ArrayList<>();
            sasaran3.add(TipeWarga.ANAK_ANAK);

            List<TipeWarga> sasaran4 = new ArrayList<>();
            sasaran4.add(TipeWarga.IBU_IBU);

            Kegiatan kegiatan1 = new Kegiatan("Kerja Bakti", "Musholla", date1,
                    "Membersihkan lingkungan", "Kerja Bakti di Musholla pada hari sabtu",
                    sasaran1, 1, 1);

            Kegiatan kegiatan2 = new Kegiatan("Rapat Karang Taruna", "Rumah Maguire", date2,
                    "Rapat rutin", "Rapat di Rumah Maguire, bahas regenerasi",
                    sasaran2, 2, 1);

            Kegiatan kegiatan3 = new Kegiatan("Lomba Mewarnai", "Rumah Belajar", date3,
                    "Dalam rangka ulang tahun desa", "Lomba Mewarnai di Rumah belajar untuk anak anak",
                    sasaran3, 3, 1);

            Kegiatan kegiatan4 = new Kegiatan("Memasak untuk syukuran", "Rumah Bu Ami", date4,
                    "Dalam rangka syukuran diterimanya Phil Jones di Universitas London",
                    "Memasak untuk syukuran di Rumah Bu Ami Syukuran Phil Jones",
                    sasaran4, 4, 1);

            kegiatanDao.insert(kegiatan1);
            kegiatanDao.insert(kegiatan2);
            kegiatanDao.insert(kegiatan3);
            kegiatanDao.insert(kegiatan4);

            User user1 = new User("Damar Wardoyo", "I Love Liverpool", TipeWarga.BAPAK_BAPAK,
                    1);
            User user2 = new User("Ahmad Nur Barraza", "#GGMU", TipeWarga.PEMUDA,
                    1);
            User user3 = new User("Nabila Fathia", "Cintaimu ususmu", TipeWarga.IBU_IBU,
                    1);
            User user4 = new User("Khoirul Kuluq", "#Respec", TipeWarga.ANAK_ANAK,
                    1);

            userDao.insert(user1);
            userDao.insert(user2);
            userDao.insert(user3);
            userDao.insert(user4);

            List<Integer> list = new ArrayList<>();
            list.add(1);
            list.add(2);
            list.add(3);
            list.add(4);

            Kampung kampung = new Kampung("Kampung Warna Warni", "Sidomulyo Karanggeneng Boyolali",
                    1, 2, list);

            kampungDao.insert(kampung);

            return null;
        }
    }
}
