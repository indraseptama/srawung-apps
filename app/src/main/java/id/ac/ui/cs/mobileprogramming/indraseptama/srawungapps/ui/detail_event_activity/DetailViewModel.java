package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.detail_event_activity;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.CommentRepository;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.KegiatanRepository;

public class DetailViewModel extends AndroidViewModel {
    private KegiatanRepository mRepo;
    private CommentRepository commentRepository;
    public DetailViewModel(@NonNull Application application) {
        super(application);
        mRepo = new KegiatanRepository(application);
        commentRepository = new CommentRepository(application);
    }

    public LiveData<Kegiatan> getKegiatanById(String id){
        return mRepo.getKegiatanById(id);
    }

    public void updateKegiatan(Kegiatan kegiatan){
        mRepo.update(kegiatan);
    }

    public void insertComment(Comment comment) {
        commentRepository.insert(comment);
    }

    public LiveData<Comment> getCommentById(String id){
        return commentRepository.getCommentById(id);
    }

    public LiveData<List<Comment>> getAllComment(){
        return commentRepository.getAllComment();
    }
}
