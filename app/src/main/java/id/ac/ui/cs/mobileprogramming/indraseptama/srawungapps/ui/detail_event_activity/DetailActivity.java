package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.detail_event_activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.adapter.RecycleViewCommentAdapter;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;

public class DetailActivity extends AppCompatActivity {
    private static final String TAG = "DetailActivity";
    private DetailViewModel detailViewModel;
    final int REQUEST_CODE_GALLERY = 999;
    final int REQUEST_IMAGE_CAPTURE = 1;
    private Uri selectedUri;
    private Kegiatan kegiatan;
    private RecyclerView mRecycleView;
    private RecycleViewCommentAdapter mAdapter;
    private List<Comment> comments = new ArrayList<>();
    private int commentSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        try{
            MediaManager.init(DetailActivity.this);
        }catch (IllegalStateException e){

        }
        Intent intent = this.getIntent();
        String id = intent.getStringExtra("kegiatanId");
        Button submit = findViewById(R.id.button_unggah);
        mRecycleView = findViewById(R.id.recycleComment);
        initRecycleView();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        DetailActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        detailViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);

        detailViewModel.getAllComment().observe(this, new Observer<List<Comment>>() {
            @Override
            public void onChanged(List<Comment> comments) {
                commentSize = comments.size();
            }
        });

        detailViewModel.getKegiatanById(id).observe(this, new Observer<Kegiatan>() {
            @Override
            public void onChanged(Kegiatan kegiatan1) {
                setView(kegiatan1);
                kegiatan = kegiatan1;
                if(kegiatan1.getIdComment() != null){
                    for(Integer id : kegiatan1.getIdComment()){
                        detailViewModel.getCommentById(String.valueOf(id))
                                .observe(DetailActivity.this, new Observer<Comment>() {
                            @Override
                            public void onChanged(Comment comment) {
                                if(comment!=null){
                                    comments.add(comment);
                                    mAdapter.setmComment(comments);
                                }
                            }
                        });
                    }
                }
            }
        });
    }



    public void setView(Kegiatan mKegiatan) {
        String patternDay = "EEEE, dd MMMM yyyy";
        String patternHours = "HH:mm";

        SimpleDateFormat sdfDay = new SimpleDateFormat(patternDay);
        SimpleDateFormat sdfHours = new SimpleDateFormat(patternHours);

        TextView namaKegiatan = findViewById(R.id.nama_kegiatan);
        TextView descShortkegiatan = findViewById(R.id.desc_short_kegiatan);
        TextView hari = findViewById(R.id.hari_kegiatan);
        TextView tempat = findViewById(R.id.tempat_kegiatan);
        TextView sasaran = findViewById(R.id.sasaran_kegiatan);
        TextView alasan = findViewById(R.id.alasanKegiatan);
        TextView dibuatOleh = findViewById(R.id.dibuatOleh);
        TextView jam = findViewById(R.id.jam_kegiatan);

        namaKegiatan.setText(mKegiatan.getName());
        descShortkegiatan.setText(mKegiatan.getShortDescription());
        hari.setText(sdfDay.format(mKegiatan.getDate()));
        jam.setText(sdfHours.format(mKegiatan.getDate()));

        tempat.setText(mKegiatan.getPlace());
        sasaran.setText("");

        for(TipeWarga tipe: mKegiatan.getSasaran()){
            if(tipe.equals(TipeWarga.IBU_IBU)){
                sasaran.append("- Ibu-ibu\n");
            }
            else if(tipe.equals(TipeWarga.ANAK_ANAK)){
                sasaran.append("- Anak-anak\n");
            }
            else if(tipe.equals(TipeWarga.BAPAK_BAPAK)){
                sasaran.append("- Bapak-bapak\n");
            }
            else if(tipe.equals(TipeWarga.PEMUDA)){
                sasaran.append("- Pemuda\n");
            }
        }
        sasaran.setText(sasaran.getText().subSequence(0,sasaran.getText().length()-1));
        alasan.setText(mKegiatan.getReason());
        dibuatOleh.setText(String.valueOf(mKegiatan.getUserID()));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(),
                        "You don't have permission to access file location!",
                        Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            selectedUri = data.getData();

            AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);

            builder
                    .setMessage(getString(R.string.dialogConfirmation))
                    .setPositiveButton(getString(R.string.Ya),  new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            submit();
                        }
                    })
                    .setNegativeButton(getString(R.string.Tidak), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void submit(){
        MediaManager.get().upload(selectedUri)
                .unsigned("rdv1xotm")
                .option("resource_type", "image")
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {

                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        String cloudinaryUrl = resultData.get("secure_url").toString();
                        Comment comment = new Comment("Indras", cloudinaryUrl);
                        List<Integer> idComment = kegiatan.getIdComment();
                        if(idComment == null){
                            idComment = new ArrayList<>();
                        }
                        idComment.add(commentSize+1);
                        kegiatan.setIdComment(idComment);
                        detailViewModel.insertComment(comment);
                        detailViewModel.updateKegiatan(kegiatan);
                        Log.d(TAG, "onSuccess: ");
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                }).dispatch();
    }


    private void initRecycleView(){
        mAdapter = new RecycleViewCommentAdapter();
        RecyclerView.LayoutManager linearLayout = new LinearLayoutManager(DetailActivity.this);
        mRecycleView.setLayoutManager(linearLayout);
        mRecycleView.setAdapter(mAdapter);
    }
}