package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

public class AlertReceiver extends BroadcastReceiver {
    private static final String KEY_NAMA = "nama";
    private static final String KEY_DESCRIPTION = "desc";

    @Override
    public void onReceive(Context context, Intent intent) {
        String nama = intent.getStringExtra(KEY_NAMA);
        String desc = intent.getStringExtra(KEY_DESCRIPTION);
        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder builder = notificationHelper.getChannel1Notification(nama, desc);
        notificationHelper.getNotificationManager().notify(1, builder.build());


    }
}
