package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.next_event;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.KegiatanRepository;

public class NextEventViewModel extends AndroidViewModel {
    private static final String TAG = "NextEventViewModel";
    private LiveData<List<Kegiatan>> mKegiatan;
    private KegiatanRepository mRepo;

    public NextEventViewModel(@NonNull Application application) {
        super(application);
        mRepo = new KegiatanRepository(application);
        mKegiatan = mRepo.getFutureKegiatan();
    }

    public LiveData<List<Kegiatan>> getAllKegiatans(){
        return mKegiatan;
    }
}