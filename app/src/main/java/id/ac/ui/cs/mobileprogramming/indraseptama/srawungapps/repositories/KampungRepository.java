package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.KampungDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.database.KegiatanDatabase;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kampung;

public class KampungRepository {
    private KampungDao kampungDao;
    private LiveData<Kampung> kampungLiveData;

    public KampungRepository(Application application){
        KegiatanDatabase kegiatanDatabase = KegiatanDatabase.getInstance(application);
        kampungDao = kegiatanDatabase.kampungDao();
    }

    public void insert(Kampung kampung){
        new InsertKampungAsyncTask(kampungDao).execute(kampung);
    }

    public void delete(Kampung kampung){
        new DeleteKampungAsyncTask(kampungDao).execute(kampung);
    }

    public void update(Kampung kampung){
        new UpdateKampungAsyncTask(kampungDao).execute(kampung);
    }

    public LiveData<Kampung> getKampungById(String id){
        return kampungDao.getKampungById(id);
    }

    private static class InsertKampungAsyncTask extends AsyncTask<Kampung, Void, Void> {
        private KampungDao kampungDao;

        private InsertKampungAsyncTask(KampungDao kampungDao){
            this.kampungDao = kampungDao;
        }

        @Override
        protected Void doInBackground(Kampung... kampungs) {
            kampungDao.insert(kampungs[0]);
            return null;
        }
    }

    private static class UpdateKampungAsyncTask extends AsyncTask<Kampung, Void, Void> {
        private KampungDao kampungDao;

        private UpdateKampungAsyncTask(KampungDao kampungDao){
            this.kampungDao = kampungDao;
        }

        @Override
        protected Void doInBackground(Kampung... kampungs) {
            kampungDao.update(kampungs[0]);
            return null;
        }
    }

    private static class DeleteKampungAsyncTask extends AsyncTask<Kampung, Void, Void> {
        private KampungDao kampungDao;

        private DeleteKampungAsyncTask(KampungDao kampungDao){
            this.kampungDao = kampungDao;
        }

        @Override
        protected Void doInBackground(Kampung... kampungs) {
            kampungDao.delete(kampungs[0]);
            return null;
        }
    }
}
