package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;

public class RecycleViewCommentAdapter extends RecyclerView.Adapter<RecycleViewCommentAdapter.ViewHolder> {
    private Context mContext;
    private List<Comment> mComment = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_coment, parent, false);
        mContext = parent.getContext();
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    public void setmComment(List<Comment> comments){
        this.mComment = comments;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position){
        holder.namaUser.setText(mComment.get(position).getNamaUser());
        Glide.with(mContext)
                .asBitmap()
                .load(mComment.get(position).getUrlImage())
                .into(holder.foto);
    }

    @Override
    public int getItemCount() {
        return mComment.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView namaUser;
        ImageView foto;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            namaUser = itemView.findViewById(R.id.NamaUser);
            foto = itemView.findViewById(R.id.foto);
        }
    }
}
