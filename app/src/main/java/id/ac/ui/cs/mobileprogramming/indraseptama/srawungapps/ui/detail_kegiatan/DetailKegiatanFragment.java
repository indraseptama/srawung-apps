package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.detail_kegiatan;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.MyComunicator;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;

public class DetailKegiatanFragment extends Fragment implements MyComunicator {
    private static final String TAG = "DetailKegiatanFragment";
    private DetailKegiatanViewModel mViewModel;
    private TextView namaKegiatan;
    private TextView descShortkegiatan;
    private TextView hari;
    private TextView jam;
    private TextView tempat;
    private TextView sasaran;
    private TextView alasan;
    private TextView dibuatOleh;

    public static DetailKegiatanFragment newInstance() {
        return new DetailKegiatanFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_kegiatan, container, false);
        this.namaKegiatan = view.findViewById(R.id.nama_kegiatan);
        this.descShortkegiatan = view.findViewById(R.id.desc_short_kegiatan);
        this.hari = view.findViewById(R.id.hari_kegiatan);
        this.jam = view.findViewById(R.id.jam_kegiatan);
        this.tempat = view.findViewById(R.id.tempat_kegiatan);
        this.sasaran = view.findViewById(R.id.sasaran_kegiatan);
        this.alasan = view.findViewById(R.id.alasanKegiatan);
        this.dibuatOleh = view.findViewById(R.id.dibuatOleh);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DetailKegiatanViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void displayDetails(Kegiatan mKegiatan) {
        String patternDay = "EEEE, dd MMMM yyyy";
        String patternHours = "HH:mm";

        SimpleDateFormat sdfDay = new SimpleDateFormat(patternDay);
        SimpleDateFormat sdfHours = new SimpleDateFormat(patternHours);

        this.namaKegiatan.setText(mKegiatan.getName());
        this.descShortkegiatan.setText(mKegiatan.getShortDescription());
        this.jam.setText(sdfHours.format(mKegiatan.getDate()));
        this.hari.setText(sdfDay.format(mKegiatan.getDate()));
        this.tempat.setText(mKegiatan.getPlace());
        this.sasaran.setText("");

        for(TipeWarga tipe: mKegiatan.getSasaran()){
            if(tipe.equals(TipeWarga.IBU_IBU)){
                this.sasaran.append("- Ibu-ibu\n");
            }
            else if(tipe.equals(TipeWarga.ANAK_ANAK)){
                this.sasaran.append("- Anak-anak\n");
            }
            else if(tipe.equals(TipeWarga.BAPAK_BAPAK)){
                this.sasaran.append("- Bapak-bapak\n");
            }
            else if(tipe.equals(TipeWarga.PEMUDA)){
                this.sasaran.append("- Pemuda\n");
            }
        }
        this.sasaran.setText(sasaran.getText().subSequence(0,sasaran.getText().length()-1));
        this.alasan.setText(mKegiatan.getReason());
        this.dibuatOleh.setText(String.valueOf(mKegiatan.getUserID()));
    }
}
