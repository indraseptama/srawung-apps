package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.add_event_activity;

import android.app.Application;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.KegiatanRepository;

public class AddEventViewModel extends AndroidViewModel {
    private static final String TAG = "AddEventViewModel";
    public MutableLiveData<String> nama = new MutableLiveData<>();
    public MutableLiveData<String> deskripsiSingkat = new MutableLiveData<>();
    public MutableLiveData<String> tempat = new MutableLiveData<>();
    public MutableLiveData<String> alasan = new MutableLiveData<>();
    public MutableLiveData<String> hari = new MutableLiveData<>();
    public MutableLiveData<String> jam = new MutableLiveData<>();
    public MutableLiveData<Boolean> isBapakBapakSelected = new MutableLiveData<>(false);
    public MutableLiveData<Boolean> isIbuIbuSelected = new MutableLiveData<>(false);
    public MutableLiveData<Boolean> isPemudaSelected = new MutableLiveData<>(false);
    public MutableLiveData<Boolean> isAnakAnakSelected = new MutableLiveData<>(false);
    public MutableLiveData<List<TipeWarga>> sasaran = new MutableLiveData<>();
    private MutableLiveData<Kegiatan> kegiatanMutableLiveData;
    private KegiatanRepository kegiatanRepository;

    public KegiatanRepository getKegiatanRepository() {
        return kegiatanRepository;
    }

    public AddEventViewModel(@NonNull Application application) {
        super(application);
        kegiatanRepository = new KegiatanRepository(application);
    }

    public void insert(Kegiatan kegiatan){
        kegiatanRepository.insert(kegiatan);
    }

    public MutableLiveData<Kegiatan> getKegiatan() {
        if (kegiatanMutableLiveData == null) {
            kegiatanMutableLiveData = new MutableLiveData<>();
        }
        return kegiatanMutableLiveData;
    }

    private Date getDate() {
        return new Date();
    }

    public void OnClick(View view) {
        Date date = null;

        if(!TextUtils.isEmpty(hari.getValue()) && !TextUtils.isEmpty(jam.getValue())){
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy HH:mm");
                date = sdf.parse(hari.getValue() +  " " + jam.getValue());
            }
            catch (ParseException e){}
        }

        Kegiatan kegiatan = new Kegiatan(nama.getValue(), tempat.getValue(), date,
                alasan.getValue(), deskripsiSingkat.getValue(), sasaran.getValue(), 1, 1);

        kegiatanMutableLiveData.setValue(kegiatan);

    }

    public void OnCheckBoxClick() {
        List<TipeWarga> tmp = new ArrayList<>();
        if (isBapakBapakSelected.getValue()) {
            tmp.add(TipeWarga.BAPAK_BAPAK);
        }

        if (isAnakAnakSelected.getValue()) {
            tmp.add(TipeWarga.ANAK_ANAK);
        }

        if (isIbuIbuSelected.getValue()) {
            tmp.add(TipeWarga.IBU_IBU);
        }

        if (isPemudaSelected.getValue()) {
            tmp.add(TipeWarga.PEMUDA);
        }

        sasaran.setValue(tmp);
    }
}
