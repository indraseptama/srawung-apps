package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.MyTypeConverter;

@Entity(tableName = "comment_table")
@TypeConverters(MyTypeConverter.class)
public class Comment {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String namaUser;

    private String urlImage;

    public Comment(String namaUser, String urlImage) {
        this.namaUser = namaUser;
        this.urlImage = urlImage;
    }

    public void setId(int id) {
        this.id = id;
    }



    public int getId() {
        return id;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public String getUrlImage() {
        return urlImage;
    }
}
