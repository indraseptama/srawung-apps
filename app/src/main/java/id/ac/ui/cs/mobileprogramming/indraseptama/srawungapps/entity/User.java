package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.MyTypeConverter;

@Entity(tableName = "user_table")
@TypeConverters(MyTypeConverter.class)
public class User {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private String bio;

    private TipeWarga type;

    private int kampungId;

    public User(String name, String bio, TipeWarga type, int kampungId) {
        this.name = name;
        this.bio = bio;
        this.type = type;
        this.kampungId = kampungId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getBio() {
        return bio;
    }

    public String getName() {
        return name;
    }

    public TipeWarga getType() {
        return type;
    }

    public int getKampungId() {
        return kampungId;
    }
}