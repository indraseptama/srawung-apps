package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;

public interface MyComunicator {
    public void displayDetails(Kegiatan mKegiatan);
}
