package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.MyTypeConverter;

@Entity(tableName = "table_kampung")
@TypeConverters(MyTypeConverter.class)
public class Kampung {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private int idKetuaRT;

    private int idKetuaPemuda;

    private String alamat;

    private List<Integer> idWargas = null;

    public Kampung(String name, String alamat, int idKetuaRT, int idKetuaPemuda, List<Integer> idWargas){
        this.name = name;
        this.alamat = alamat;
        this.idKetuaRT = idKetuaRT;
        this.idKetuaPemuda = idKetuaPemuda;
        this.idWargas = idWargas;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getIdKetuaRT() {
        return idKetuaRT;
    }

    public int getIdKetuaPemuda() {
        return idKetuaPemuda;
    }

    public String getAlamat() {
        return alamat;
    }

    public List<Integer> getIdWargas() {
        return idWargas;
    }
}
