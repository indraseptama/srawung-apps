package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.KegiatanDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.database.KegiatanDatabase;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;

public class KegiatanRepository {
    private static final String TAG = "KegiatanRepository";
    private KegiatanDao kegiatanDao;
    private LiveData<List<Kegiatan>> allKegiatan;

    public KegiatanRepository(Application application){
        KegiatanDatabase kegiatanDatabase = KegiatanDatabase.getInstance(application);
        kegiatanDao = kegiatanDatabase.kegiatanDao();
        allKegiatan = kegiatanDao.getAllKegiatans();
    }

    public void insert(Kegiatan kegiatan){
        new InsertKegiatanAsyncTask(kegiatanDao).execute(kegiatan);
    }

    public void delete(Kegiatan kegiatan){
        new DeleteKegiatanAsyncTask(kegiatanDao).execute(kegiatan);
    }

    public void update(Kegiatan kegiatan){
        new UpdateKegiatanAsyncTask(kegiatanDao).execute(kegiatan);
    }

    public void deleteAll(){
        new DeleteAllKegiatanAsyncTask(kegiatanDao).execute();
    }

    public LiveData<Kegiatan> getKegiatanById(String id){
        return kegiatanDao.getKegiatanById(id);
    }

    public LiveData<List<Kegiatan>> getFutureKegiatan(){
        return  kegiatanDao.getFutureEvent();
    }

    public LiveData<List<Kegiatan>> getPastKegiatan(){
        return  kegiatanDao.getPastEvent();
    }

    public LiveData<List<Kegiatan>> getAllKegiatan(){
        return  allKegiatan;
    }

    private static class InsertKegiatanAsyncTask extends AsyncTask<Kegiatan, Void, Void>{
        private KegiatanDao kegiatanDao;

        private InsertKegiatanAsyncTask(KegiatanDao kegiatanDao){
            this.kegiatanDao = kegiatanDao;
        }

        @Override
        protected Void doInBackground(Kegiatan... kegiatans) {
            kegiatanDao.insert(kegiatans[0]);
            return null;
        }
    }

    private static class UpdateKegiatanAsyncTask extends AsyncTask<Kegiatan, Void, Void>{
        private KegiatanDao kegiatanDao;

        private UpdateKegiatanAsyncTask(KegiatanDao kegiatanDao){
            this.kegiatanDao = kegiatanDao;
        }

        @Override
        protected Void doInBackground(Kegiatan... kegiatans) {
            kegiatanDao.update(kegiatans[0]);
            return null;
        }
    }

    private static class DeleteKegiatanAsyncTask extends AsyncTask<Kegiatan, Void, Void>{
        private KegiatanDao kegiatanDao;

        private DeleteKegiatanAsyncTask(KegiatanDao kegiatanDao){
            this.kegiatanDao = kegiatanDao;
        }

        @Override
        protected Void doInBackground(Kegiatan... kegiatans) {
            kegiatanDao.delete(kegiatans[0]);
            return null;
        }
    }

    private static class DeleteAllKegiatanAsyncTask extends AsyncTask<Void, Void, Void>{
        private KegiatanDao kegiatanDao;

        private DeleteAllKegiatanAsyncTask(KegiatanDao kegiatanDao){
            this.kegiatanDao = kegiatanDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            kegiatanDao.deleteAllKegiatan();
            return null;
        }
    }
}