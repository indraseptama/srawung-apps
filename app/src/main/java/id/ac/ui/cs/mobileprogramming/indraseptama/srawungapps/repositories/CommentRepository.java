package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.CommentDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.database.KegiatanDatabase;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;

public class CommentRepository {
    private CommentDao commentDao;
    private LiveData<List<Comment>> allComment;

    public CommentRepository(Application application){
        KegiatanDatabase kegiatanDatabase = KegiatanDatabase.getInstance(application);
        commentDao = kegiatanDatabase.commentDao();
        allComment = commentDao.getAllComents();
    }

    public LiveData<List<Comment>> getAllComment(){
        return allComment;
    }

    public void insert(Comment comment){
        new InsertCommentAsyncTask(commentDao).execute(comment);
    }

    public LiveData<Comment> getCommentById(String id){
        return commentDao.getCommentById(id);
    }

    private static class InsertCommentAsyncTask extends AsyncTask<Comment, Void, Void> {
        private CommentDao commentDao;

        private InsertCommentAsyncTask(CommentDao commentDao){
            this.commentDao = commentDao;
        }

        @Override
        protected Void doInBackground(Comment... comments) {
            commentDao.insert(comments[0]);
            return null;
        }
    }
}
