package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao.UserDao;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.database.KegiatanDatabase;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.User;

public class UserRepository {
    private UserDao userDao;

    public UserRepository(Application application){
        KegiatanDatabase kegiatanDatabase = KegiatanDatabase.getInstance(application);
        userDao = kegiatanDatabase.userDao();
    }

    public LiveData<User> getUserById(String id){
        return userDao.getUserById(id);
    }
}
