package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.kampung;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kampung;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.User;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.KampungRepository;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.repositories.UserRepository;

public class KampungkuViewModel extends AndroidViewModel {
    public MutableLiveData<String> nama = new MutableLiveData<>();
    public MutableLiveData<String> alamat = new MutableLiveData<>();
    public MutableLiveData<String> namaKetuaRT = new MutableLiveData<>();
    public MutableLiveData<String> namaKetuaPemuda = new MutableLiveData<>();
    private KampungRepository kampungRepository;
    private UserRepository userRepository;

    public MutableLiveData<String> wargas = new MutableLiveData<>("");

    public KampungkuViewModel(Application application){
        super(application);
        kampungRepository = new KampungRepository(application);
        userRepository = new UserRepository(application);
    }

    public LiveData<Kampung> getKampung(String id){
        LiveData<Kampung> kampungLiveData =  kampungRepository.getKampungById(id);
        return  kampungLiveData;
    }

    public LiveData<User> getWargas(String s) {
        return userRepository.getUserById(s);
    }

    public User getUserById(String id){
        return userRepository.getUserById(id).getValue();
    }

}
