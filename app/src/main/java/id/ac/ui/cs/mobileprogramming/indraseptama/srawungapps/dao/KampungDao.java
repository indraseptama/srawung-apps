package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kampung;

@Dao
public interface KampungDao {
    @Insert
    void insert(Kampung kampung);

    @Update
    void update(Kampung kampung);

    @Delete
    void delete(Kampung kampung);

    @Query("DELETE FROM table_kampung")
    void deleteAllKampung();

    @Query("SELECT * FROM table_kampung")
    LiveData<List<Kampung>> getAllKampungs();

    @Query("SELECT * FROM table_kampung WHERE id=:id")
    LiveData<Kampung> getKampungById(String id);
}
