package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.past_event;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.adapter.RecycleViewEventAdapter;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;

public class PastEventFragment extends Fragment {

    private PastEventViewModel mViewModel;
    private RecyclerView mRecycleView;
    private RecycleViewEventAdapter mAdapter;

    public static PastEventFragment newInstance() {
        return new PastEventFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_event, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecycleView();
        mViewModel = ViewModelProviders.of(this).get(PastEventViewModel.class);
        mViewModel.getAllKegiatans().observe(this, new Observer<List<id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan>>() {
            @Override
            public void onChanged(List<Kegiatan> kegiatans) {
                mAdapter.setmKegiatans(kegiatans);
            }
        });

    }

    private void initRecycleView(){
        mAdapter = new RecycleViewEventAdapter();
        mRecycleView = getView().findViewById(R.id.recycle_view);
        RecyclerView.LayoutManager linearLayout = new LinearLayoutManager(getContext());
        mRecycleView.setLayoutManager(linearLayout);
        mRecycleView.setAdapter(mAdapter);
    }

}
