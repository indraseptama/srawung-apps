package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.TipeWarga;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.MyTypeConverter;


@Entity(tableName = "kegiatan_table")
@TypeConverters(MyTypeConverter.class)
public class Kegiatan {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    private String place;

    private Date date;

    private String reason;

    private String shortDescription;

    private int userID;

    private int kampungID;

    private List<TipeWarga> sasaran = null;

    private List<Integer> idComment = null;

    public Kegiatan(String name, String place, Date date, String reason, String shortDescription, List<TipeWarga> sasaran, int userID, int kampungID) {
        this.name = name;
        this.place = place;
        this.date = date;
        this.reason = reason;
        this.shortDescription = shortDescription;
        this.sasaran = sasaran;
        this.userID = userID;
        this.kampungID = kampungID;
    }

    public void setIdComment(List<Integer> idComment) {
        this.idComment = idComment;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public Date getDate() {
        return date;
    }

    public String getReason() {
        return reason;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public int getUserID() {
        return userID;
    }

    public int getKampungID() {
        return kampungID;
    }

    public List<TipeWarga> getSasaran() {
        return sasaran;
    }

    public List<Integer> getIdComment() {
        return idComment;
    }
}
