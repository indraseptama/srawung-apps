package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Comment;

public class Nyoba extends AppCompatActivity {
    final int REQUEST_CODE_GALLERY = 999;
    private Uri selectedUri;
    private ImageView holderImage;
    private Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nyoba);
        Button button = findViewById(R.id.button);
        holderImage = findViewById(R.id.imgHolderUpload);
        button1 = findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        Nyoba.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            String cloudinaryUrl;
            @Override
            public void onClick(View v) {
                MediaManager.init(Nyoba.this);
                MediaManager.get().upload(selectedUri)
                        .unsigned("rdv1xotm")
                        .option("resource_type", "image")
                        .callback(new UploadCallback() {
                            @Override
                            public void onStart(String requestId) {
                                Log.d("CLOUDINARY", "onStart: starting.....");
                            }

                            @Override
                            public void onProgress(String requestId, long bytes, long totalBytes) {

                            }

                            @Override
                            public void onSuccess(String requestId, Map resultData) {
                                cloudinaryUrl = resultData.get("secure_url").toString();
                                Log.d("CLOUDINARY", "onSuccess: succeed");
                                Log.d("CLOUDINARY", "onSuccess: "+ cloudinaryUrl);


                             //   bukuViewModel.insert(new Buku(judul,penulis, lokasi, keterangan, imgStr, cloudinaryUrl));
                            }

                            @Override
                            public void onError(String requestId, ErrorInfo error) {

                            }

                            @Override
                            public void onReschedule(String requestId, ErrorInfo error) {

                            }
                        }).dispatch();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            selectedUri = data.getData();

            AlertDialog.Builder builder = new AlertDialog.Builder(Nyoba.this);

            builder
                    .setMessage(getString(R.string.dialogConfirmation))
                    .setPositiveButton(getString(R.string.Ya),  new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    })
                    .setNegativeButton(getString(R.string.Tidak), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
