package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;

@Dao
public interface KegiatanDao {

    @Insert
    void insert(Kegiatan kegiatan);

    @Update
    void update(Kegiatan kegiatan);

    @Delete
    void delete(Kegiatan kegiatan);

    @Query("DELETE FROM kegiatan_table")
    void deleteAllKegiatan();

    @Query("SELECT * FROM kegiatan_table ORDER BY date DESC")
    LiveData<List<Kegiatan>> getAllKegiatans();

    @Query("SELECT * FROM kegiatan_table WHERE datetime(date/1000, 'unixepoch', 'localtime') > datetime('now', 'localtime')  ORDER BY date ASC")
    LiveData<List<Kegiatan>> getFutureEvent();

    @Query("SELECT * FROM kegiatan_table WHERE datetime(date/1000, 'unixepoch', 'localtime') < datetime('now', 'localtime') ORDER BY date DESC")
    LiveData<List<Kegiatan>> getPastEvent();

    @Query("SELECT * FROM kegiatan_table WHERE id=:id")
    LiveData<Kegiatan> getKegiatanById(String id);
}