package id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.ui.next_event;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.utils.AlertReceiver;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.R;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.adapter.RecycleViewEventAdapter;
import id.ac.ui.cs.mobileprogramming.indraseptama.srawungapps.entity.Kegiatan;

public class NextEventFragment extends Fragment {
    private static final String TAG = "NextEventFragment";
    private static final String KEY_NAMA = "nama";
    private static final String KEY_DESCRIPTION = "desc";
    private NextEventViewModel nextEventViewModel;
    private RecyclerView mRecycleView;
    private RecycleViewEventAdapter mAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_event, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecycleView();

        nextEventViewModel = ViewModelProviders.of(this).get(NextEventViewModel.class);
        nextEventViewModel.getAllKegiatans().observe(this, new Observer<List<Kegiatan>>() {
            @Override
            public void onChanged(List<Kegiatan> kegiatans) {
                if (kegiatans != null && !kegiatans.isEmpty()) {
                    mAdapter.setmKegiatans(kegiatans);
                    Calendar calendar = Calendar.getInstance();
                    Kegiatan kegiatan = kegiatans.get(0);
                    calendar.setTime(kegiatan.getDate());
                    startAlarm(kegiatan.getName(), kegiatan.getShortDescription(), calendar);
                }
            }
        });
    }

    private void initRecycleView() {
        mAdapter = new RecycleViewEventAdapter();
        mRecycleView = getView().findViewById(R.id.recycle_view);
        RecyclerView.LayoutManager linearLayout = new LinearLayoutManager(getContext());
        mRecycleView.setLayoutManager(linearLayout);
        mRecycleView.setAdapter(mAdapter);
    }

    private void startAlarm(String nama, String desc, Calendar calendar) {
        AlarmManager alarmManager = (AlarmManager) getActivity()
                .getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getContext(), AlertReceiver.class);
        intent.putExtra(KEY_NAMA, nama);
        intent.putExtra(KEY_DESCRIPTION, desc);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 1, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }
}